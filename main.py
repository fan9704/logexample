import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(message)s",
    datefmt="%Y-%m-%d %H:%M",
    handlers=[logging.FileHandler('test.log','w','utf-8'),]
)
logging.debug("HELLO DEBUG")
logging.info("HELLO INFO")
logging.warning("HELLO WARNING")
logging.error("HELLO ERROR")
logging.critical("HELLO CRITICAL")

from logging.config import fileConfig
fileConfig("logging_config.ini")
logger = logging.getLogger()
logger.debug("TOO FAT")